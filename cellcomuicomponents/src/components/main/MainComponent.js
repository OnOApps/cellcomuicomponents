'use strict';

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Dimensions, Animated, TouchableOpacity, Text, Platform} from 'react-native';
import * as actions from '../../actions';
import  LogoComponent from './LogoComponent';
import  Toggle  from './Toggle';

let toggleToTopAnim = null;

class MainComponent extends Component {

  state = {
    isRight: true,
    activeTopAnimation : false,
    position: new Animated.Value(0),
    toggleDuration: null,
    componentDuration: null
  };

  componentWillMount(){
    toggleToTopAnim = {transform:[
      {
        translateY: this.state.position.interpolate({
          inputRange: [0, 0.8, 1],
          outputRange: [90, 60, 70]
        })
      }
    ]};
  }

  animate(){
    this.setState({
      activeTopAnimation : !this.state.activeTopAnimation
    });

    if(this.state.activeTopAnimation){
      Animated.timing(
        this.state.position,
        {
          toValue: 0,
          duration: 400
        }
      ).start();
    }
    else {
      Animated.timing(
        this.state.position,
        {
          toValue: 1,
          duration: 400
        }
      ).start();
    }
  }

  render() {
    const {isRight, activeTopAnimation} = this.state;

    return (
      <View style={styles.container}>
        <LogoComponent
          activeTopAnimation={activeTopAnimation}
          componentDuration={400}
        />
        <Toggle
          customStyle={styles.toggleComponent}
          leftText={"לשירותך"}
          rightText={"מידע אישי"}
          isRight={isRight}
          toggleColor="rgb(135,218,221)"
          textColor="rgb(144,148,144)"
          textStyle={{fontSize: 17}}
          toggleLeft={() => this.setState({isRight: false})}
          toggleRight={() => this.setState({isRight: true})}
          activeTopAnimation={activeTopAnimation}
          toggleDuration={300}
          componentDuration={400}
        />
          <Animated.View style={[styles.boxStyle, toggleToTopAnim]}>
              <TouchableOpacity style={{flex: 1, justifyContent: 'center', alignItems: 'center'}} onPress={() => {this.animate()}}>
                <Text style={styles.textStyle}>Click for Animation</Text>
              </TouchableOpacity>
          </Animated.View>
      </View>
    );
  }
}

const {height, width} = Dimensions.get('window');

const styles = {
  container: {
    flex: 1,
    alignItems: 'center'
  },
  boxStyle: {
    width: width * 0.9,
    height: height * 0.5,
    borderWidth: 1
  },
  toggleComponent:{
    position: 'absolute',
    marginTop: height * 0.10,
    justifyContent: 'center'
  },
  textStyle: {
    fontSize: 20,
    color: 'purple'
  }
};

const mapStateToProps = state => {
  const { dummyData } = state.dummy;

  return { dummyData };
};

export default connect(mapStateToProps, actions)(MainComponent);
