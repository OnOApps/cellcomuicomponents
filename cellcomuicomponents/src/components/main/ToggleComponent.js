'use strict';

import React from 'react';
import { View, Text, LayoutAnimation, TouchableWithoutFeedback, Dimensions, Platform} from 'react-native';
import {BoxShadow} from 'react-native-shadow';
import { Actions } from 'react-native-router-flux';
import * as Animatable from 'react-native-animatable';

const SCREEN_WIDTH = Dimensions.get('window').width;

const Toggle = ({ leftText, rightText, activeText, toggleSide, togglePosition, toggleColor, onPress }) => {
  const shadowOpt = {
    width: SCREEN_WIDTH * 0.75,
    height:42,
    color:"#000",
    border: 5,
    radius: 21,
    opacity: 0.15,
    x:0,
    y:3,
    style:{marginVertical: 10}
  };

  if(Platform.OS === 'ios') {
    return (
      <View style={[styles.toggleWrapper, togglePosition]}>
        <View style={[styles.toggleContainer]}>
          <TouchableWithoutFeedback style={styles.touchableStyles} onPress={onPress}>
            <View style={[styles.toggleSubContainer, {borderColor: toggleColor}]}>
              <View style={styles.fixedTextContainer}>
                <Text style={[styles.toggleText, {color:"red", zIndex: 150}]}>{leftText}</Text>
              </View>
              <View style={styles.fixedTextContainer}>
                <Text style={[styles.toggleText, {color:"red", zIndex: 150}]}>{rightText}</Text>
              </View>
            </View>
          </TouchableWithoutFeedback>
          <View style={[styles.toggleActivePart, toggleSide, {backgroundColor: toggleColor}]}>
            {/*<Text style={styles.toggleText}>{activeText}</Text>*/}
          </View>
        </View>
      </View>
    );
  }else{
    return (
      <View style={[styles.toggleWrapper, togglePosition]}>
        <BoxShadow setting={shadowOpt}>
          <View style={[styles.toggleContainer]}>
            <TouchableWithoutFeedback style={styles.touchableStyles} onPress={onPress}>
              <View style={[styles.toggleSubContainer, {borderColor: toggleColor}]}>
                <View style={styles.fixedTextContainer}>
                  <Text style={[styles.toggleText, {color:toggleColor}]}>{leftText}</Text>
                </View>
                <View style={styles.fixedTextContainer}>
                  <Text style={[styles.toggleText, {color:toggleColor}]}>{rightText}</Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
            <View style={[styles.toggleActivePart, toggleSide, {backgroundColor: toggleColor}]}>
            </View>
          </View>
        </BoxShadow>
      </View>
    );
  }
};

const styles = {
  toggleWrapper: {
    position: 'absolute',
    height: Platform.OS === 'ios' ? 42 : 62,
    backgroundColor: 'transparent',
    flexDirection: 'row'
  },
  toggleContainer: {
    height: 42,
    width: SCREEN_WIDTH * 0.75,
    borderRadius: 21,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 10 },
    shadowOpacity: 0.2,
    shadowRadius: 10
  },
  toggleSubContainer: {
    flexDirection: 'row',
    height: 42,
    alignSelf: 'stretch',
    backgroundColor: '#fff',
    borderRadius: 21,
    borderWidth: 1,
  },
  touchableStyles: {
    height: 42,
    width: SCREEN_WIDTH * 0.75,
    zIndex: 150
  },
  fixedTextContainer: {
    flex: 1,
    height: 42,
    borderRadius: 21,
    justifyContent: 'center',
    alignItems: 'center'
  },
  toggleText: {
    // fontFamily: Platform.OS === 'ios' ? 'ALMONIDLAAA' : 'almoni-dl-aaa-regular',
    fontSize: 18,
    color: '#fff',
  },
  toggleActivePart: {
    position: 'absolute',
    width: SCREEN_WIDTH * 0.375,
    height: 34,
    borderRadius: 21,
    zIndex: 100,
    marginTop : 4,
    justifyContent: 'center',
    alignItems: 'center'
  }
};

export default Toggle ;
