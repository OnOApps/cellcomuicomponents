'use strict';

import React, { Component } from 'react';
import { View,Text, Animated, LayoutAnimation, Dimensions, TouchableWithoutFeedback, Platform} from 'react-native';
import GestureRecognizer from 'react-native-swipe-gestures';
import {BoxShadow} from 'react-native-shadow';


let leftColor = null;
let rightColor = null;
let toggleToTopAnim = null;

class Toggle extends Component {

  state = {
    value: new Animated.Value(0),
    position: new Animated.Value(0),
    rightPosition: new Animated.Value(3)
  };

  componentWillMount(){
    const {textColor} = this.props;

    leftColor = this.state.value.interpolate({
      inputRange: [0, 1],
      outputRange: [textColor, 'rgba(255, 255, 255, 1)']
    });

    rightColor = this.state.value.interpolate({
      inputRange: [0, 1],
      outputRange: ['rgba(255, 255, 255, 1)', textColor]
    });

    toggleToTopAnim = {transform:[{
      scale: this.state.position.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0.8],
      })
    },
      {
        translateY: this.state.position.interpolate({
          inputRange: [0, 0.8, 1],
          outputRange: [0, -70, -60]
        })
      }
    ]};
  }

  componentWillReceiveProps(nextProp){
    const {value} = this.state;
    const {isRight} = this.props;

    if (this.props.isRight != nextProp.isRight){
      if(!isRight) {
        Animated.timing(
          this.state.rightPosition,
          {
            toValue: 3,
            duration: this.props.toggleDuration
          }
        ).start();
      }else{
        Animated.timing(
          this.state.rightPosition,
          {
            toValue: (width * 0.65)/2 + 3,
            duration: this.props.toggleDuration
          }
        ).start();
      }

      Animated.timing(
        value,
        {
          toValue: !isRight ? 0 : 1,
          duration: this.props.toggleDuration
        }
      ).start();
    }

    if(nextProp.activeTopAnimation){
      Animated.timing(
        this.state.position,
        {
          toValue: 1,
          duration: this.props.componentDuration
        }
      ).start();
    }
    else {
      Animated.timing(
        this.state.position,
        {
          toValue: 0,
          duration: this.props.componentDuration
        }
      ).start();
    }
  }

  onSwipe(gestureName){
    this.setState({gestureName: gestureName});
  }

  render() {

    const {toggleWrapper, componentToggleContainer, ToggleContainer, ActiveToggle, ToggleText} = styles;
    const {isRight, toggleLeft, toggleRight, leftText, rightText, toggleColor, textStyle, customStyle} = this.props;

    const shadowOpt = {
      width: width * 0.64,
      height:40,
      color:"#E9E9E9",
      border: 5,
      radius: 21,
      opacity: 0.5,
      x:0,
      y:3,
      style:{marginVertical: 68}
    };

    if(Platform.OS === 'ios') {
      return (
        <Animated.View style={[componentToggleContainer, customStyle, toggleToTopAnim]}>
          <GestureRecognizer
            onSwipe={(direction, state) => this.onSwipe(direction, state)}
            config={{
              velocityThreshold: 0.4,
              directionalOffsetThreshold: 200
            }}
            onSwipeLeft={() => {
              toggleLeft();
            }}
            onSwipeRight={() => {
              toggleRight();
            }}
          >
            <View style={ToggleContainer}>
              <TouchableWithoutFeedback onPress={() => {
                toggleLeft();
              }}>
                <View style={styles.textContainer}>
                  <Animated.Text style={[ToggleText, textStyle, {
                    color: leftColor,
                    fontWeight: isRight ? "normal" : "bold"
                  }]}>{leftText}</Animated.Text>
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => {
                toggleRight();
              }}>
                <View style={styles.textContainer}>
                  <Animated.Text style={[ToggleText, textStyle, {
                    color: rightColor,
                    fontWeight: isRight ? "bold" : "normal"
                  }]}>{rightText}</Animated.Text>
                </View>
              </TouchableWithoutFeedback>
              <Animated.View style={[ActiveToggle, {backgroundColor: toggleColor}, {right: this.state.rightPosition}]} />
            </View>
          </GestureRecognizer>
        </Animated.View>
      );
    }
    else{
      return (
        <View style={toggleWrapper}>
          <Animated.View style={[componentToggleContainer, customStyle, toggleToTopAnim]}>
            <GestureRecognizer
              onSwipe={(direction, state) => this.onSwipe(direction, state)}
              config={{
                velocityThreshold: 0.4,
                directionalOffsetThreshold: 180
              }}
              onSwipeLeft={() => {
                this.press(1);
                toggleLeft()
              }}
              onSwipeRight={() => {
                this.press(0);
                toggleRight()
              }}
            >
              <View style={ToggleContainer}>
                <TouchableWithoutFeedback onPress={() => {
                    this.press(1);
                    toggleLeft();
                }}>
                  <View style={styles.textContainer}>
                    <Animated.Text style={[ToggleText, textStyle, {
                      color: leftColor,
                      fontWeight: isRight ? "normal" : "bold"
                    }]}>{leftText}</Animated.Text>
                  </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {
                    this.press(0);
                    toggleRight();
                }}>
                  <View style={styles.textContainer}>
                    <Animated.Text style={[ToggleText, textStyle, {
                      color: rightColor,
                      fontWeight: isRight ? "bold" : "normal"
                    }]}>{rightText}</Animated.Text>
                  </View>
                </TouchableWithoutFeedback>
                <Animated.View style={[ActiveToggle, {backgroundColor: toggleColor}, {right: this.state.rightPosition}]}/>
              </View>
            </GestureRecognizer>
          </Animated.View>
          <Animated.View style={[{position:'absolute'}, toggleToTopAnim]}>
            <BoxShadow setting={shadowOpt}></BoxShadow>
          </Animated.View>
        </View>
      )
    }
  }
}


const {height, width} = Dimensions.get('window');

const  styles = {
  toggleWrapper:{
    position:'relative',
    alignItems : 'center'
  },
  componentToggleContainer:{
    width: width * 0.65,
    height: 40,
    backgroundColor : 'white',
    borderRadius:40,
    shadowColor: 'rgb(233, 233, 233)',
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.5,
    zIndex: 100
  },
  textContainer: {
    backgroundColor: 'transparent',
    flex: 1,
    alignItems: 'center',
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  ToggleContainer: {
    flexDirection: 'row',
    height: 32,
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  ActiveToggle: {
    position: 'absolute',
    width: width * 0.31,
    height: 32,
    bottom: 0,
    borderRadius:40,
    borderWidth: 0,
    zIndex: -2,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1
  },
  ToggleText:{
    fontSize: 17,
    height:  Platform.OS === 'ios' ? 40 : 30,
    lineHeight: Platform.OS === 'ios' ? 40 : 25,
    alignSelf: 'stretch',
    textAlign: 'center'
  }
}


export default Toggle;