'use strict';

import React, { Component } from 'react';
import { View, LayoutAnimation, Animated, Image} from 'react-native';

class LogoComponent extends Component {

  state = {
    width: new Animated.Value(65),
    imageWidth: new Animated.Value(30)
  }

  componentWillUpdate() {
    // LayoutAnimation.configureNext({duration: 600, update: {type: LayoutAnimation.Types.easeInEaseOut}});
  }

  componentWillReceiveProps(nextProp){
      if (nextProp.activeTopAnimation) {
        Animated.timing(
          this.state.width,
          {
            toValue: 30,
            duration: this.props.componentDuration
          }
        ).start();
        Animated.timing(
          this.state.imageWidth,
          {
            toValue: 0,
            duration: this.props.componentDuration
          }
        ).start();
      }
      else {
        Animated.timing(
          this.state.width,
          {
            toValue: 65,
            duration: this.props.componentDuration
          }
        ).start();
        Animated.timing(
          this.state.imageWidth,
          {
            toValue: 30,
            duration: this.props.componentDuration
          }
        ).start();
      }
  }

  render() {
    const { activeTopAnimation } = this.props;
    const { container, textStyle, imageStyle} = styles;

    return (
      <Animated.View style={[container, {width: this.state.width}]}>
        <Animated.Image style={[imageStyle, {left:5, width:30, height:10}, {width: this.state.imageWidth} ]} source={require('../../asset/Cellcom_Logo_cellcom.png')}/>
        <Image style={imageStyle} source={require('../../asset/star.png')}/>
      </Animated.View>
    );
  }
}

const  styles = {
  container: {
    marginTop: 20,
    left: 10,
    position: 'absolute',
    backgroundColor: 'purple',
    borderBottomRightRadius: 13,
    borderBottomLeftRadius: 13,
    paddingVertical: 2,
    paddingHorizontal: 5,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30
  },
  textStyle:{
    color: 'white',
    fontSize: 14,
    position: 'absolute',
    left: 5,
    backgroundColor: 'transparent'
  },
  imageStyle:{
    width: 18,
    height: 18,
    position: 'absolute',
    right: 5
  }
};


export default LogoComponent;